TARGET = aroundview
QT = quickcontrols2
QT += widgets
QT += quick gui aglextras


SOURCES = main.cpp \
			camera.cpp \
			capturing.cpp \


HEADERS += camera.h \
			capturing.h \

RESOURCES += AroundView.qrc\
				images/images.qrc

INCLUDEPATH += /usr/include/opencv2
DEPENDPATH += /usr/include/opencv2

LIBS += `pkg-config opencv --libs`

include(app.pri)
