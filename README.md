# Overview

Aroundview application is a sample application.
It's a simple application that takes into the camera input and draws them like all around view using v4l2 technology.

The follow image is the screenshot of this application.


![screenshot_png](img/aroundview_screenshot.png)

## Compile

- Execute the following command on the host PC.
    >$source <path to sdk>/environment-setup-aarch64-agl-linux  (e.g. /opt/agl-sdk/m3ulcb/environtment-setup-aarch64-agl-linux)
    >
    >$mkdir build
    >
    >$cd build
    >
    >$qmake ../
    >
    >$make

- Wiget file will be genarated in the following directory.
    > aroundview/build/package/aroundview.wgt

## Operating procedure

- Hardware
    - Target board : R-Car M3 + Kingfisher
    - GMSL Camera Board : SBEV-RCAR-KF-GMSL02 from company SIMAFUJI
    - Camera : four cameras from company IMI
    (※Discontinued product)

- Constitution
    - AGL image version is halibut 7.99.1.

    ![constitusion_png](img/aroundview_constitution.png)

- Install
    - Copy aroundview.wgt from hostPC to target.
    - Execute following command.
    >#afm-util install aroundview.wgt

- Startup
Click the "A" icon on the Homescreen.

    ![icon_png](img/icon.png)

## Note

- In this application, the images can't be composite like they are concatenationed.
- In this application, the images can't be combined as they are connected.
